function snapCrackle(maxValue) {
    let result = ""
    for (let counter = 1; counter < maxValue; counter += 1) {
        if (counter % 2 !== 0 && counter % 5 === 0) {
                result += "SnapCrackle, "
                console.log("SnapCrackle, ")
            }
        else if (counter % 2 !== 0) {
            result += "Snap, "
            console.log("Snap, ")
        }
        else if (counter % 5 === 0) {
            result += "Crackle, "
            console.log("Crackle, ")
        }
        else {
            result += (counter + ", ")
            console.log(counter + ", ")
        }
    }

    return result
}

console.log(snapCrackle(100))